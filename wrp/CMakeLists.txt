project(_wrp)

pybind11_add_module(_wrp MODULE LabelledTetrahedralMeshWrapper.cpp)

target_include_directories(_wrp PRIVATE ${CMAKE_SOURCE_DIR}/cpp/)
target_link_libraries(_wrp PUBLIC _cpp)

install(TARGETS _wrp DESTINATION pybind_example)