#include <pybind11/pybind11.h>
#include <pybind11/stl.h> // This header is needed to work with STL containers like std::vector
#include <pybind11/eigen.h>

#include "CgalLabelledImage.h"
#include "LabelledTetrahedralMesh.h"

namespace py = pybind11;


int cgalLabelledImage(Eigen::Array<unsigned short, Eigen::Dynamic, 1> image_array, Eigen::Array<int, 3, 1> shape, Eigen::Array<double, 3, 1> voxelsize)
{
    CGAL::Image_3 image = buildCgalImage(image_array, shape, voxelsize);
    if ((image.xdim() != shape(0)) || (image.ydim() != shape(1)) || (image.zdim() != shape(2))) {
        return EXIT_FAILURE;
    }
    return 0;
}

PYBIND11_MODULE(_wrp, m) {
    py::class_<LabelledTetrahedralMesh>(m, "LabelledTetrahedralMesh")
        .def(py::init<>())
        .def("vertex_points", &LabelledTetrahedralMesh::vertexPoints)
        .def("add_point", &LabelledTetrahedralMesh::addPoint, py::arg("point"))
        .def("tetra_vertices", &LabelledTetrahedralMesh::tetraVertices)
        .def("tetra_labels", &LabelledTetrahedralMesh::tetraLabels)
        .def("add_tetra", &LabelledTetrahedralMesh::addTetra, py::arg("tetra"), py::arg("label"));

    m.def("make_tetrahedron", &makeTetrahedron);

    m.def("build_cgal_image", &cgalLabelledImage, py::arg("image_array"), py::arg("shape"), py::arg("voxelsize"));
}