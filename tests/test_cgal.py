import unittest
import tempfile
import os

from pybind_example.cgal import mesh_3d_image


class TestCgalMesh(unittest.TestCase):

    def setUp(self):
        self.image_filename = tempfile.mktemp(suffix=".inr.gz")
        self.mesh_filename = tempfile.mktemp(suffix=".medit")

    def tearDown(self):
        if os.path.exists(self.image_filename):
            os.remove(self.image_filename)
        if os.path.exists(self.mesh_filename):
            os.remove(self.mesh_filename)

    def test_mesh_3d_image(self):
        res = mesh_3d_image(self.image_filename, self.mesh_filename)
        assert res == 1 # should not work for now, test that it fails
        assert not os.path.exists(self.mesh_filename)
