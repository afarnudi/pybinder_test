from __future__ import annotations

import pybind_example as m
import numpy as np
import pytest

def test_version():
    assert m.__version__ == "0.0.1"


def test_add():
    assert m.add(1, 2) == 3


def test_sub():
    assert m.subtract(1, 2) == -1


def test_sum_array_numpy_array_1D_float():
    array = np.asarray([1.0, 1.0, 2.0, 1.0, 1.0])
    assert m.sum_array(array) == 6


def test_sum_array_numpy_list_1D_float():
    list_ = [1.0, 1.0, 2.0, 1.0, 1.0]
    assert m.sum_array(list_) == 6


def test_sum_array_numpy_array_1D_int():
    array = np.ones(10)
    assert m.sum_array(array) == 10


def test_sum_array_numpy_array_2D_float():
    array = np.arange(9).reshape((3, 3))
    assert m.sum_2d_array(array) == np.sum(array)


def test_sum_rows_numpy_array():
    array = np.arange(9).reshape((3, 3))
    np.testing.assert_array_almost_equal(m.sum_rows(array), np.sum(array, axis=1))


def test_matrix_multiply_1():
    array_1 = np.asarray([[1, 2], [3, 4]])
    array_2 = np.asarray([[5, 6], [7, 8]])
    np.testing.assert_array_almost_equal(
        m.matrix_multiply(array_1, array_2), array_1 @ array_2
    )

def test_matrix_multiply_raise():
    array_1 = np.asarray([[1, 2, 6], [3, 4, 5]])
    array_2 = np.asarray([[5, 6, 1], [7, 8, 1]])
    with pytest.raises(ValueError) as e:
        m.matrix_multiply(array_1, array_2)
        assert str(e) == "Matrices cannot be multiplied due to incompatible dimensions"
    
