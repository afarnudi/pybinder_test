import unittest

import numpy as np

from pybind_example.labelled_tetrahedral_mesh import build_cgal_image


class TestCgalImage(unittest.TestCase):

    def setUp(self):
        self.image = np.zeros((10, 8, 6), dtype=np.uint16)
        self.image[:, :, 4:] = 1
        self.image[:, :4, :4] = 2
        self.image[:, 4:, :4] = 3

        self.voxelsize = [0.5, 0.5, 1]

    def tearDown(self):
        pass

    def test_cgal_image(self):
        res = build_cgal_image(self.image, voxelsize=self.voxelsize)
        assert res == 0
