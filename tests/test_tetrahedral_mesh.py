import unittest

from pybind_example.labelled_tetrahedral_mesh import LabelledTetrahedralMesh


class TestTetrahedralMesh(unittest.TestCase):

    def test_tetrahedral_mesh_init(self):
        mesh = LabelledTetrahedralMesh()
        assert mesh is not None

    def test_tetrahedral_mesh_points(self):
        mesh = LabelledTetrahedralMesh()
        mesh.add_point([0, 0, 0])
        points = mesh.vertex_points()
        assert points.shape == (1, 3)

    def test_tetrahedral_mesh_tetras(self):
        mesh = LabelledTetrahedralMesh()
        mesh.add_point([0, 0, 0])
        mesh.add_point([1, 0, 0])
        mesh.add_point([0, 1, 0])
        mesh.add_point([0, 0, 1])
        points = mesh.vertex_points()
        assert points.shape == (4, 3)

        mesh.add_tetra([0, 1, 2, 3], 0)
        tetras = mesh.tetra_vertices()
        assert tetras.shape == (1, 4)

        tetra_labels = mesh.tetra_labels()
        assert tetra_labels.shape == (1,)
        assert tetra_labels[0] == 0
