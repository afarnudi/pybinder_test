#pragma once

#include <CGAL/Image_3.h>

#include <Eigen/Dense>


CGAL::Image_3 buildCgalImage(Eigen::Array<unsigned short, Eigen::Dynamic, 1> image_array, Eigen::Array<int, 3, 1> shape, Eigen::Array<double, 3, 1> voxelsize={1.0, 1.0, 1.0});