#include <iostream>

#include "CgalLabelledImage.h"


CGAL::Image_3 buildCgalImage(Eigen::Array<unsigned short, Eigen::Dynamic, 1> image_array, Eigen::Array<int, 3, 1> shape, Eigen::Array<double, 3, 1> voxelsize)
{
    _image* image = _createImage(
        shape(0), shape(1), shape(2), 1,
        voxelsize(0), voxelsize(1), voxelsize(2), 2,
        WK_FIXED, SGN_UNSIGNED
    );

    auto it = image_array.begin();
    for (int x=0; x<shape(0); x++) {
        for (int y=0; y<shape(1); y++) {
            for (int z=0; z<shape(2); z++) {
                CGAL::IMAGEIO::static_evaluate<unsigned short>(image, x, y, z) = *it;
                ++it;
            }
        }
    }

    return CGAL::Image_3(image);
}
