#pragma once

#include <vector>
#include <Eigen/Dense>

class LabelledTetrahedralMesh
{
public:
     LabelledTetrahedralMesh(void) = default;
    ~LabelledTetrahedralMesh(void) = default;

public:
    Eigen::ArrayX3d vertexPoints(void) const;
    void addPoint(Eigen::Array3d point);

    Eigen::Array<unsigned short, Eigen::Dynamic, 4> tetraVertices(void) const;
    Eigen::Array<unsigned short, Eigen::Dynamic, 1> tetraLabels(void) const;
    void addTetra(Eigen::Array<unsigned short, 4, 1>  tetra, unsigned short label);

private:
    std::vector< std::vector<double> > _vertex_points;
    std::vector< std::vector<unsigned short> > _tetra_vertices;
    std::vector< unsigned short > _tetra_labels;
};

LabelledTetrahedralMesh makeTetrahedron(void);