#include "LabelledTetrahedralMesh.h"


Eigen::ArrayX3d LabelledTetrahedralMesh::vertexPoints(void) const
{
    Eigen::ArrayX3d vertex_points(this->_vertex_points.size(), 3);
    for (int i=0; i<this->_vertex_points.size(); i++) {
        for (int dim=0; dim<3; dim++) {
            vertex_points(i, dim) = this->_vertex_points[i][dim];
        }
    }
    return vertex_points;
}

void LabelledTetrahedralMesh::addPoint(Eigen::Array3d point)
{
    std::vector<double> _point = std::vector<double> {point(0), point(1), point(2)};
    this->_vertex_points.push_back(_point);
}

Eigen::Array<unsigned short, Eigen::Dynamic, 4> LabelledTetrahedralMesh::tetraVertices(void) const
{
    Eigen::Array<unsigned short, Eigen::Dynamic, 4> tetra_vertices(this->_tetra_vertices.size(), 4);
    for (int i=0; i<this->_tetra_vertices.size(); i++) {
        for (int v=0; v<4; v++) {
            tetra_vertices(i, v) = this->_tetra_vertices[i][v];
        }
    }
    return tetra_vertices;
}

Eigen::Array<unsigned short, Eigen::Dynamic, 1> LabelledTetrahedralMesh::tetraLabels(void) const
{
    Eigen::Array<unsigned short, Eigen::Dynamic, 1> tetra_labels(this->_tetra_labels.size(), 1);
    for (int i=0; i<this->_tetra_labels.size(); i++) {
        tetra_labels(i, 0) = this->_tetra_labels[i];
    }
    return tetra_labels;
}

void LabelledTetrahedralMesh::addTetra(Eigen::Array<unsigned short, 4, 1>  tetra, unsigned short label)
{
    std::vector<unsigned short> _tetra = std::vector<unsigned short> {tetra(0), tetra(1), tetra(2), tetra(3)};
    this->_tetra_vertices.push_back(_tetra);
    this->_tetra_labels.push_back(label);
}


LabelledTetrahedralMesh makeTetrahedron(void)
{
    LabelledTetrahedralMesh m;
    m.addPoint({0., 0., 0.});
    m.addPoint({1., 0., 0.});
    m.addPoint({0., 1., 0.});
    m.addPoint({0., 0., 1.});

    m.addTetra({0, 1, 2, 3}, 0);

    return m;
}
