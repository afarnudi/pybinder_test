from __future__ import annotations

from ._core import (
    __doc__,
    __version__,
    add,
    subtract,
    multiply,
    sum_array,
    sum_2d_array,
    sum_rows,
    matrix_multiply,
)

__all__ = [
    "__doc__",
    "__version__",
    "add",
    "subtract",
    "multiply",
    "sum_array",
    "sum_2d_array",
    "sum_rows",
    "matrix_multiply",
]
