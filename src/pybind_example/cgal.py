from __future__ import annotations

from ._cgal import __doc__, mesh_3d_image

__all__ = [
    "__doc__",
    "mesh_3d_image"
]
