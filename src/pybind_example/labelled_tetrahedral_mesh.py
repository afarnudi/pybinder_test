from typing import Iterable, Optional

import numpy as np

from ._wrp import LabelledTetrahedralMesh, make_tetrahedron
from ._wrp import build_cgal_image as _build_cgal_image


def build_cgal_image(img: np.ndarray, voxelsize: Optional[Iterable[float]]=(1., 1., 1.)) -> bool:
    return _build_cgal_image(img.ravel().astype(np.uint16), img.shape, voxelsize)
