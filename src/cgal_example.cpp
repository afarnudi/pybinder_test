#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>

#include <CGAL/Mesh_triangulation_3.h>
#include <CGAL/Mesh_complex_3_in_triangulation_3.h>
#include <CGAL/Mesh_criteria_3.h>

#include <CGAL/Labeled_image_mesh_domain_3.h>
#include <CGAL/make_mesh_3.h>
#include <CGAL/Image_3.h>

#include <CGAL/ImageIO.h>

#include <pybind11/pybind11.h>
#include <pybind11/stl.h> // This header is needed to work with STL containers like std::vector

// Domain
typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Labeled_mesh_domain_3<K> Mesh_domain;

// Triangulation
typedef CGAL::Mesh_triangulation_3<Mesh_domain, CGAL::Default, CGAL::Sequential_tag>::type Tr;
typedef CGAL::Mesh_complex_3_in_triangulation_3<Tr> C3t3;

// Criteria
typedef CGAL::Mesh_criteria_3<Tr> Mesh_criteria;


int cgalMesh3DImage(const std::string& image_filename, const std::string& mesh_filename)
{
    CGAL::Image_3 image;
    if(!image.read(image_filename)){
        std::cerr << "Error: Cannot read file " <<  image_filename << std::endl;
        return EXIT_FAILURE;
    }
    Mesh_domain domain = Mesh_domain::create_labeled_image_mesh_domain(image);

    // Mesh criteria
    auto params = CGAL::parameters::facet_angle(30);
    params.facet_size(6);
    params.facet_distance(4);
    params.cell_radius_edge_ratio(3);
    params.cell_size(8);
    Mesh_criteria criteria(params);

    C3t3 c3t3 = CGAL::make_mesh_3<C3t3>(domain, criteria);

    // Output
    std::ofstream medit_file(mesh_filename);
    CGAL::IO::write_MEDIT(medit_file, c3t3);
    medit_file.close();
    return 0;
}


namespace py = pybind11;

PYBIND11_MODULE(_cgal, m) {
    m.doc() = R"pbdoc(
        CGAL example function
        ---------------------

        .. currentmodule:: pybind_example.cgal

        .. autosummary::
           :toctree: _generate

           add
           subtract
    )pbdoc";

    m.def("mesh_3d_image", &cgalMesh3DImage, R"pbdoc(
        Domains From Segmented 3D Images

        The function produces a 3D mesh from a 3D image.
    )pbdoc", py::arg("image_filename"), py::arg("mesh_filename"));

}
