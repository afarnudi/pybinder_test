#include <pybind11/pybind11.h>
#include <pybind11/stl.h> // This header is needed to work with STL containers like std::vector

// #define VERSION_INFO 1.0

#define STRINGIFY(x) #x
#define MACRO_STRINGIFY(x) STRINGIFY(x)

int add(int i, int j) {
    return i + j;
}

double multiply(double i, double j) {
    return i * j;
}

double sum_array(const std::vector<double> &arr) {
    double sum = 0;
    for (double num : arr) {
        sum += num;
    }
    return sum;
}

double sum_2d_array(const std::vector<std::vector<double>> &arr) {
    double sum = 0;
    for (const auto &row : arr) {
        for (double num : row) {
            sum += num;
        }
    }
    return sum;
}

std::vector<double> sum_rows(const std::vector<std::vector<double>> &arr) {
    std::vector<double> row_sums;
    for (const auto &row : arr) {
        double row_sum = 0;
        for (double num : row) {
            row_sum += num;
        }
        row_sums.push_back(row_sum);
    }
    return row_sums;
}

std::vector<std::vector<double>> matrix_multiply(const std::vector<std::vector<double>> &arr1, const std::vector<std::vector<double>> &arr2) {
    int m1 = arr1.size();
    int n1 = arr1[0].size();
    int m2 = arr2.size();
    int n2 = arr2[0].size();

    if (n1 != m2) {
        throw std::invalid_argument("Matrices cannot be multiplied due to incompatible dimensions");
    }
    std::vector<std::vector<double>> result(m1, std::vector<double>(n2, 0));

    for (int i = 0; i < m1; ++i) {
        for (int j = 0; j < n2; ++j) {
            for (int k = 0; k < n1; ++k) {
                result[i][j] += arr1[i][k] * arr2[k][j];
            }
        }
    }

    return result;
}


namespace py = pybind11;

PYBIND11_MODULE(_core, m) {
    m.doc() = R"pbdoc(
        Pybind11 example plugin
        -----------------------

        .. currentmodule:: pybind_example

        .. autosummary::
           :toctree: _generate

           add
           subtract
    )pbdoc";

    m.def("add", &add, R"pbdoc(
        Add two numbers

        Some other explanation about the add function.
    )pbdoc", py::arg("i"), py::arg("j"));
    m.def("multiply", &multiply, R"pbdoc(
        Multiply two numbers

        Some other explanation about the multiply function.
    )pbdoc",py::arg("i"), py::arg("j"));

    m.def("subtract", [](int i, int j) { return i - j; }, R"pbdoc(
        Subtract two numbers

        Some other explanation about the subtract function.
    )pbdoc",py::arg("i"), py::arg("j"));

    m.def("sum_array", &sum_array, "Calculate the sum of elements in an array");
    m.def("sum_2d_array", &sum_2d_array, "Calculate the sum of elements in a 2D array");
    m.def("sum_rows", &sum_rows, "Calculate the sum of elements in each row of a 2D array");
    m.def("matrix_multiply", &matrix_multiply, "Perform matrix multiplication of two 2D arrays");

#ifdef VERSION_INFO
    m.attr("__version__") = MACRO_STRINGIFY(VERSION_INFO);
#else
    m.attr("__version__") = "dev";
#endif
}
